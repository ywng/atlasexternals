# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test
import unittest
import six

@unittest.skipIf(not six.PY2, "Not run for Python3")
class Test(Flake8Test):
   """
   Test print statement checks
   """

   def test1(self):
      """Check print statements"""
      from flake8_atlas.python23 import print_statement as checker
      self.assertFail('print "Hello"', checker)
      self.assertPass('print("Hello")', checker)

   def test2(self):
      """Check incompatible print statements"""
      from flake8_atlas.python23 import incompatible_print_statement as checker
      self.assertFail('print("a","b")', checker)
      self.assertPass('print(1)', checker)
      self.assertPass('print ("a")', checker)
      self.assertPass('print("%s %s" % ("a","b"))', checker)
      self.assertFail('print', checker)
