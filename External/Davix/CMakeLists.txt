# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Davix as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( Davix )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_DAVIX )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Davix as part of this project" )

# The source code of Davix:
set( _source "http://cern.ch/lcgpackages/tarFiles/sources/davix-0.7.1.tar.gz" )
set( _md5 "aa731b0b346b0e1b18a1a5c62aef7aa8" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/DavixBuild )

# Package(s) required for the build.
find_package( UUID REQUIRED )
find_package( OpenSSL REQUIRED )

# Extra arguments for the build configuration:
set( _extraArgs )
if( ATLAS_BUILD_LIBXML2 )
   set( _lib "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}" )
   set( _lib "${_lib}/${CMAKE_SHARED_LIBRARY_PREFIX}" )
   set( _lib "${_lib}xml2${CMAKE_SHARED_LIBRARY_SUFFIX}" )
   list( APPEND _extraArgs
      -DLIBXML2_INCLUDE_DIR:PATH=${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/libxml2
      -DLIBXML2_LIBRARY:PATH=${_lib}
      -DLIBXML2_LIBRARIES:PATH=${_lib} )
else()
   find_package( LibXml2 )
   list( APPEND _extraArgs
      -DLIBXML2_INCLUDE_DIR:PATH=${LIBXML2_INCLUDE_DIR}
      -DLIBXML2_LIBRARY:PATH=${LIBXML2_LIBRARY}
      -DLIBXML2_LIBRARIES:PATH=${LIBXML2_LIBRARIES} )
endif()
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs
      -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build Davix:
ExternalProject_Add( Davix
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   CONFIGURE_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   ${CMAKE_COMMAND}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DUUID_INCLUDE_DIR:PATH=${UUID_INCLUDE_DIR}
   -DUUID_LIBRARY:FILEPATH=${UUID_uuid_LIBRARY}
   -DOPENSSL_INCLUDE_DIR:PATH=${OPENSSL_INCLUDE_DIR}
   -DOPENSSL_SSL_LIBRARY:FILEPATH=${OPENSSL_SSL_LIBRARY}
   -DOPENSSL_CRYPTO_LIBRARY:FILEPATH=${OPENSSL_CRYPTO_LIBRARY}
   -DLIB_SUFFIX:STRING= ${_extraArgs} <SOURCE_DIR>
   BUILD_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make
   INSTALL_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make install
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Davix forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Davix"
   DEPENDERS download )
ExternalProject_Add_Step( Davix purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Davix"
   DEPENDEES download
   DEPENDERS patch )
if( ATLAS_BUILD_LIBXML2 )
   ExternalProject_Add_Step( Davix forceconfigure
      COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
      COMMENT "Forcing the configuration of Davix"
      DEPENDEES update
      DEPENDERS configure
      ALWAYS 1 )
endif()
ExternalProject_Add_Step( Davix buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Davix into the build area"
   DEPENDEES install )
add_dependencies( Package_Davix Davix )
if( ATLAS_BUILD_LIBXML2 )
   add_dependencies( Davix LibXml2 )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Davix Python )
endif()

# Install Davix:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
