# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthSimulationExternals.
#
+ External/CLHEP
+ External/FastJet
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GeoModelCore
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/MKL
+ External/PyModules
+ External/dSFMT
+ External/flake8_atlas
+ External/nlohmann_json
+ External/prmon
+ External/yampl
- .*
